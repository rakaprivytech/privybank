#okhttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.okhttp.internal.huc.**
-dontwarn okio.**
-dontwarn org.xmlpull.v1.**
-dontwarn java.lang.invoke***
-dontwarn com.squareup.**

#viger
-dontwarn  com.necistudio.vigerpdf.core
-dontwarn  com.necistudio.vigerpdf.core.Stepper
-dontwarn  com.necistudio.vigerpdf.core.MuPDFCancellableTaskDefinition
-dontwarn  com.necistudio.vigerpdf.core.CancellableTaskDefinition
-dontwarn  com.necistudio.vigerpdf.core.CancellableAsyncTask
-dontwarn  com.necistudio.vigerpdf.core.MuPDFCore
-dontwarn  com.artifex.mupdf.**
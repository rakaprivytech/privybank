#okhttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn com.squareup.okhttp.internal.huc.**
-dontwarn okio.**
-dontwarn org.xmlpull.v1.**
-dontwarn java.lang.invoke***
-dontwarn com.squareup.**

package com.necistudio.vigerpdf;

import android.content.Context;
import android.graphics.Bitmap;

import com.necistudio.vigerpdf.item.Data;
import com.necistudio.vigerpdf.manage.OnResultListener;
import com.necistudio.vigerpdf.manage.RenderingPDF;
import com.necistudio.vigerpdf.manage.RenderingPDFNetwork;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Vim on 1/31/2017.
 */

public class VigerPDF {

    private static OnResultListener onResultListener;
    private RenderingPDFNetwork renderingPDFNetwork;

    public VigerPDF() {
        renderingPDFNetwork = new RenderingPDFNetwork();
    }

    public void vigerFromNetwork(String endpoint, OnResultListener resultListener) {
        this.onResultListener = resultListener;
        renderingPDFNetwork.init(endpoint);
    }

    public static Data setData(Data data) {
        onResultListener.resultData(data);
        return data;
    }

    public static int progressData(int progress) {
        onResultListener.progressData(progress);
        return progress;
    }
    public static Data finish(Data data){
        onResultListener.finish(data);
        return data;
    }

    public static Throwable failedData(Throwable t) {
        onResultListener.failed(t);
        return t;
    }

    public void onCancle() {
        if (renderingPDFNetwork != null) {
            renderingPDFNetwork.onCancle();
        }
    }
    public void onDestroy() {
        if (renderingPDFNetwork != null) {
            renderingPDFNetwork.onDestroy();
        }
    }

    public void clearCache(){
        if(renderingPDFNetwork!=null){
            renderingPDFNetwork.clearCache();
        }
    }
}

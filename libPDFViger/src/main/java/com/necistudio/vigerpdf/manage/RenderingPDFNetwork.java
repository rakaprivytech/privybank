package com.necistudio.vigerpdf.manage;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.necistudio.vigerpdf.VigerPDF;
import com.necistudio.vigerpdf.network.RestClient;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Vim on 1/31/2017.
 */

public class RenderingPDFNetwork {
    private CompositeDisposable compositeDisposable;
    private byte[] pathdata;
    private boolean status = false;
    private RenderingPDF renderingPDF;
    private Call<ResponseBody> call;

    public void init(String endpoint) {
        if (pathdata != null) {
            renderingPDF = new RenderingPDF(pathdata);
        } else {
            if(call == null){
                compositeDisposable = new CompositeDisposable();
                final RestClient.ApiInterface service = RestClient.getClient();
                call = service.streamFile(endpoint);
            }
            if(call!=null){
                if(!call.isExecuted()){
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            writeResponseBodyToDisk(response.body());
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            pathdata = null;
                            VigerPDF.failedData(t);
                        }
                    });
                }
            }
        }

    }

    private void writeResponseBodyToDisk(final ResponseBody body) {
        compositeDisposable.add((Disposable) Observable
                .create(new ObservableOnSubscribe<Integer>() {
                    @Override
                    public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                        try {
                            if (body == null) {
                                status = false;
                                pathdata = null;
                                e.onError(new Throwable("Error"));
                                e.onComplete();
                                compositeDisposable.clear();
                            } else {
                                InputStream inputStream = null;
                                ByteArrayOutputStream outputStream = null;
                                try {
                                    byte[] fileReader = new byte[4096];
                                    long fileSize = body.contentLength();
                                    long fileSizeDownloaded = 0;
                                    inputStream = body.byteStream();
                                    outputStream = new ByteArrayOutputStream((int) fileSize);
                                    while (true) {
                                        if (status) {
                                            status = false;
                                            pathdata = null;
                                            e.onComplete();
                                            break;
                                        } else {
                                            int read = inputStream.read(fileReader);
                                            if (read == -1) {
                                                break;
                                            }
                                            outputStream.write(fileReader, 0, read);
                                            fileSizeDownloaded += read;
                                            int progDownload = (int) ((fileSizeDownloaded * 100) / fileSize);
                                            e.onNext(progDownload);
                                        }
                                    }
                                    outputStream.flush();
                                    pathdata = outputStream.toByteArray();
                                    e.onComplete();

                                } catch (Exception ee) {
                                    pathdata = null;
                                    e.onError(ee);
                                    e.onComplete();
                                    compositeDisposable.clear();
                                } finally {
                                    if (inputStream != null) {
                                        inputStream.close();
                                    }
                                    if (outputStream != null) {
                                        outputStream.close();
                                    }
                                }

                            }
                        } catch (Exception ee) {
                            pathdata = null;
                            e.onError(ee);
                            e.onComplete();
                            compositeDisposable.clear();
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Integer>() {
                    @Override
                    public void onNext(Integer s) {
                        if (!status) {
                            VigerPDF.progressData(s);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        VigerPDF.failedData(e);
                    }

                    @Override
                    public void onComplete() {
                        if (pathdata != null){
                            renderingPDF = new RenderingPDF(pathdata);
                        }
                    }
                }));
    }

    public void clearCache() {
        pathdata = null;
        if (renderingPDF != null) {
            renderingPDF.clearCache();
        }
    }

    public void onCancle() {
        //status = true;
        if (renderingPDF != null) renderingPDF.onCancle();
    }

    public void onDestroy(){
        status = true;
        if (compositeDisposable != null){
            compositeDisposable.clear();
            compositeDisposable.dispose();
        }
    }
}
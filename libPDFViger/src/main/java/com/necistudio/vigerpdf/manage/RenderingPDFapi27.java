package com.necistudio.vigerpdf.manage;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;

import com.necistudio.vigerpdf.VigerPDF;
import com.necistudio.vigerpdf.core.MuPDFCore;
import com.necistudio.vigerpdf.item.Data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vim on 1/31/2017.
 */

public class RenderingPDFapi27 {
    private boolean status = false;
    private Data data;
    private CompositeDisposable compositeDisposable;
    private Bitmap bitmap;

    public RenderingPDFapi27(byte[] file) {
        compositeDisposable = new CompositeDisposable();
        initProc(file);
    }

    private void initProc(final byte[] file) {
        compositeDisposable.add(Observable.create(new ObservableOnSubscribe<Data>() {
            @Override
            public void subscribe(final ObservableEmitter<Data> e) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        File f = new File("");
                        ParcelFileDescriptor fileDescriptor = ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
                        PdfRenderer pdfRenderer = new PdfRenderer(fileDescriptor);
                        for (int i = 0; i < pdfRenderer.getPageCount(); i++) {
                            if (status) {
                                status = false;
                                e.onComplete();
                            } else {
                                    PdfRenderer.Page page = pdfRenderer.openPage(i);

                                    final int widht = page.getWidth();
                                    final int height = page.getHeight();
                                    Bitmap bitmap = Bitmap.createBitmap(widht, height,
                                            Bitmap.Config.ARGB_8888);
                                    page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                                    boolean rotate;
                                    if (widht > height) {
                                        rotate = true;
                                    } else {
                                        rotate = false;
                                    }
                                    if (rotate == true) {
                                        Matrix matrix = new Matrix();
                                        matrix.postRotate(360);
                                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, widht, height, matrix, true);
                                    }
                                    //setdatacomputation
                                    data = new Data();
                                    data.setBitmap(bitmap);
                                    data.setHeight(String.valueOf(height));
                                    data.setWidth(String.valueOf(widht));
                                    data.setSumpage(pdfRenderer.getPageCount());
                                    data.setPage(i + 1);
                                    data.setFile(null);
                                    e.onNext(data);
                            }
                        }
                        e.onComplete();
                    }
                } catch (Exception ee) {
                    e.onError(ee);
                    e.onComplete();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Data>() {
                    @Override
                    public void onNext(Data data) {
                        if (!status) {
                            VigerPDF.setData(data);
                            VigerPDF.progressData((data.getPage() * 100) / data.getSumpage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        VigerPDF.failedData(e);
                    }

                    @Override
                    public void onComplete() {
                        if (data != null) {
                            VigerPDF.finish(data);
                        }
                    }
                }));
    }

    public void clearCache() {
        data = null;
    }

    public void onCancle() {
        compositeDisposable.clear();
        compositeDisposable.dispose();
        status = true;
    }

}
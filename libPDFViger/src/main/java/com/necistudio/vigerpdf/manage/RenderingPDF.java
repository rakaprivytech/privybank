package com.necistudio.vigerpdf.manage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.necistudio.vigerpdf.VigerPDF;
import com.necistudio.vigerpdf.core.MuPDFCore;
import com.necistudio.vigerpdf.item.Data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.net.Socket;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vim on 1/31/2017.
 */

public class RenderingPDF {
    private boolean status = false;
    private Data data;
    private CompositeDisposable compositeDisposable;
    private Bitmap bitmap;

    public RenderingPDF(byte[] file) {
        compositeDisposable = new CompositeDisposable();
        initProc(file);
    }

    private void initProc(final byte[] file) {
        compositeDisposable.add(Observable.create(new ObservableOnSubscribe<Data>() {
            @Override
            public void subscribe(final ObservableEmitter<Data> e) {
                MuPDFCore core = new MuPDFCore(file, "pdf");
                try {
                    for (int i = 0; i < core.countPages(); i++) {
                        if (status) {
                            status = false;
                            e.onComplete();
                        } else {
                            final int widht = (int) core.getPageSize(i).x;
                            final int height = (int) core.getPageSize(i).y;
                            bitmap = Bitmap.createBitmap(widht, height, Bitmap.Config.ARGB_4444);
                            bitmap.compress(Bitmap.CompressFormat.JPEG,70,new ByteArrayOutputStream());

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB &&
                                    Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                                bitmap.eraseColor(0);
                            core.updatePage(bitmap, i, widht, height, 0, 0, widht, height, null);

                            boolean rotate;
                            if (widht > height) {
                                rotate = true;
                            } else {
                                rotate = false;
                            }
                            if (rotate == true) {
                                Matrix matrix = new Matrix();
                                matrix.postRotate(360);
                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                            }

                            //setdatacomputation
                            data = new Data();
                            data.setBitmap(bitmap);
                            data.setHeight(String.valueOf(height));
                            data.setWidth(String.valueOf(widht));
                            data.setSumpage(core.countPages());
                            data.setPage(i + 1);
                            data.setFile(null);
                            e.onNext(data);
                        }
                    }
                    core.onDestroy();
                    e.onComplete();
                } catch (Exception ee) {
                    e.onError(ee);
                    e.onComplete();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Data>() {
                    @Override
                    public void onNext(Data data) {
                        if (!status) {
                            VigerPDF.setData(data);
                            VigerPDF.progressData((data.getPage() * 100) / data.getSumpage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        VigerPDF.failedData(e);
                    }

                    @Override
                    public void onComplete() {
                        if (data != null) {
                            VigerPDF.finish(data);
                        }
                    }
                }));
    }

    public void clearCache() {
        data = null;
    }

    public void onCancle() {
        compositeDisposable.clear();
        compositeDisposable.dispose();
        status = true;
    }

}
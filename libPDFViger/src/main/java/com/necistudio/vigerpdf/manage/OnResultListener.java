package com.necistudio.vigerpdf.manage;

import android.graphics.Bitmap;
import android.net.Uri;

import com.necistudio.vigerpdf.item.Data;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Vim on 1/31/2017.
 */

public interface OnResultListener  {
    void resultData(Data data);
    void finish(Data data);
    void progressData(int progress);
    void failed(Throwable t);
}

package id.privy.bankprivy.utils;

import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.widget.RelativeLayout;

public class AspectRatioDevice {
    private Activity activity;

    public AspectRatioDevice(Activity activity) {
        this.activity = activity;
    }

    private String ratio() {
        Display display = activity.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        int height = size.x;
        int widht = size.y;

        final int gcd = gcd(widht, height);
        if (widht > height) {
            return showAnswer(widht / gcd, height / gcd);
        } else {
            return showAnswer(height / gcd, widht / gcd);
        }
    }

    private int gcd(int height, int widht) {
        if (widht == 0) return height;
        else return gcd(widht, height % widht);
    }

    private String showAnswer(int a, int b) {
        return a + ":" + b;
    }


    public RelativeLayout.MarginLayoutParams getAspectRationPotrait() {
        switch (ratio()) {
            case "16:9": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
            case "3:2": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT,
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT);
            }
            case "41:30": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT,
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT);
            }
            default: {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
        }
    }

    public RelativeLayout.MarginLayoutParams getAspectRationLanscape() {
        switch (ratio()) {
            case "16:9": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
            case "3:2": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
            case "41:30": {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
            default: {
                return new RelativeLayout.MarginLayoutParams(
                        RelativeLayout.MarginLayoutParams.MATCH_PARENT,
                        RelativeLayout.MarginLayoutParams.WRAP_CONTENT);
            }
        }
    }


}

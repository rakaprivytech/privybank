package id.privy.bankprivy.utils

import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {
    @GET("/upload")
    fun uploadKtp(): Deferred<Response<Any>>

    @GET("api/android")
    fun listAndroid(): Deferred<Response<Any>>
}
package id.privy.bankprivy.utils;

/**
 * Created by jarod on 12/15/14.
 */

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerDocumentAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> fragments = new ArrayList<Fragment>();
    private FragmentManager fragmentManager;

    public ViewPagerDocumentAdapter(FragmentManager fragmentManager, List<Fragment> fragments) {
        super(fragmentManager);
        this.fragmentManager = fragmentManager;
        this.fragments = fragments;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public Parcelable saveState() {
        clear();
        return super.saveState();
    }

    public void clear() {
        if (!fragments.isEmpty()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            for (Fragment fragment : fragments) {
                transaction.remove(fragment);
            }
            fragments.clear();
            transaction.commitAllowingStateLoss();
            notifyDataSetChanged();
        }
    }
}
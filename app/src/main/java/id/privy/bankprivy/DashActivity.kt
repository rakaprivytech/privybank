package id.privy.bankprivy

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.widget.Toast
import com.sequenia.navigation_router.NavigationRouter
import id.privy.bankprivy.step.*
import id.privy.bankprivy.utils.RetrofitFactory
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.GlobalScope
import kotlinx.coroutines.experimental.launch
import android.view.View
import kotlinx.android.synthetic.main.activity_dash.*
import kotlinx.coroutines.experimental.delay


class DashActivity : AppCompatActivity(),
        WelcomeFragment.WelcomeCallback,
        FormPersonalFragment.FormPersonalCallback,
        PreviewKTPFragment.PreviewKTPCallback,
        VerifiedFragment.VerifiedCallback,
        JobFragment.JobCallback,
        AccountNumberFragment.AccountCallback,
        ApprovalFragment.ApprovalCallback,
        PrivyIDFragment.PrivyCallback,
        LetterFragment.LetterCallback,
        OTPFragment.OTPCallback,
        SuccessOpenAccountFragment.SuccessOpenCallback{
    // 11. Created Account
    override fun successCreated() {

    }

    // 10. OTP
    override fun nextOtp() {
        navigator.openScreen(SuccessOpenAccountFragment.newInstance())
    }

    // 9. Sign Document
    override fun signLetter() {
        navigator.openScreen(OTPFragment.newInstance())
    }

    // 8. get PrivyID
    override fun privyGet() {
        navigator.openScreen(LetterFragment.newInstance())
    }

    // 7. Approval
    override fun approved() {
        navigator.openScreen(PrivyIDFragment.newInstance())
    }

    // 6. Account destination
    override fun backAccount() {
        navigator.closeCurrentScreen()
    }

    override fun nextAccount() {
        navigator.openScreen(ApprovalFragment.newInstance())
    }

    // 5. Job
    override fun backJob() {
        navigator.closeCurrentScreen()
    }

    override fun nextJob() {
        navigator.openScreen(AccountNumberFragment.newInstance())
    }

    // 4. Verified
    override fun backVerified() {
        navigator.closeCurrentScreen()
    }

    override fun nextVerified() {
        navigator.openScreen(JobFragment.newInstance())
    }

    // 3. Preview KTP
    override fun backKtp() {
        navigator.closeCurrentScreen()
    }

    override fun nextKtp() {
        navigator.openScreen(VerifiedFragment.newInstance())
    }

    // 2. Form Personal
    override fun personalBack() {
        navigator.closeCurrentScreen()
    }

    override fun personalNext(urlIDCard: String, urlPhoto: String) {
        navigator.openScreen(PreviewKTPFragment.newInstance(urlIDCard, urlPhoto))
    }

    // 1. Welcome Step
    override fun signIn() {
        Toast.makeText(this, "signin", Toast.LENGTH_LONG).show()
    }

    override fun signUp() {
        navigator.openScreen(FormPersonalFragment.newInstance())
    }

    private lateinit var navigator: NavigationRouter
    private var firstOpen = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash)

        navigator = NavigationRouter(this, R.id.dash_frame)

        if (firstOpen) {
            navigator.openScreen(WelcomeFragment.newInstance())
        }

        val services = RetrofitFactory.retrofitServices()
        GlobalScope.launch(Dispatchers.Main) {
            val request = services.listAndroid()
            val response = request.await()
            if (response.isSuccessful) {
                Log.e("tag", "${response.body()}")
            }
        }
    }

    private fun fakeLoading() {
        progress_indicator.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) {
            delay(500)
            progress_indicator.visibility = View.GONE
        }
    }
}

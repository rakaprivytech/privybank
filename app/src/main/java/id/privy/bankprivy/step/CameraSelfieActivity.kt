package id.privy.bankprivy.step

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import id.privy.bankprivy.R
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.CameraConfiguration
import io.fotoapparat.log.logcat
import io.fotoapparat.selector.front
import io.fotoapparat.selector.highestResolution
import io.fotoapparat.selector.manualJpegQuality
import kotlinx.android.synthetic.main.activity_camera_selfie.*
import java.io.File

class CameraSelfieActivity : AppCompatActivity() {
    private lateinit var fotoapparat: Fotoapparat
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_selfie)
        fotoapparat = Fotoapparat(
                this,
                camera_self,
                lensPosition = front(),
                cameraConfiguration = CameraConfiguration(
                        pictureResolution = highestResolution(),
                        previewResolution = highestResolution(),
                        jpegQuality = manualJpegQuality(80)
                ),
                logger = logcat(),
                cameraErrorCallback = { Toast.makeText(this, "Problem camera", Toast.LENGTH_LONG).show()}
        )

        RxView.clicks(take_selfie).subscribe {
            val photoResult = fotoapparat.takePicture()
            val dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "PrivyBank")
            if (!dir.exists()) dir.mkdirs()
            val imageStore = File(dir.absolutePath + File.separator + "SELFIE.png")
            photoResult.saveToFile(imageStore).whenAvailable {
                val intent = Intent()
                intent.putExtra("result", imageStore.absolutePath)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        fotoapparat.start()
    }

    override fun onStop() {
        super.onStop()
        fotoapparat.stop()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }
}

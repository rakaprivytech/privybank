package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_account_number.*

class AccountNumberFragment : Fragment() {
    lateinit var listener: AccountCallback
    companion object {
        fun newInstance(): AccountNumberFragment {
            val args: Bundle = Bundle()
            val fragment = AccountNumberFragment()
            fragment.arguments = args
            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account_number, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as AccountCallback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(back_dest).subscribe { listener.backAccount() }
        RxView.clicks(next_dest).subscribe { listener.nextAccount() }
    }

    interface AccountCallback{
        fun backAccount()
        fun nextAccount()
    }
}

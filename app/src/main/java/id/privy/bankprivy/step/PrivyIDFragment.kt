package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_privy_id.*

class PrivyIDFragment : Fragment() {
    private lateinit var listener: PrivyCallback
    companion object {
        fun newInstance(): PrivyIDFragment {
            val args: Bundle = Bundle()
            val fragment = PrivyIDFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener    = context as PrivyCallback
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_privy_id, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(privy_next).subscribe { listener.privyGet() }
    }
    interface PrivyCallback{
        fun privyGet()
    }
}

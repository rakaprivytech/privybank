package id.privy.bankprivy.step


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jakewharton.rxbinding2.view.RxView
import id.privy.bankprivy.utils.PermissionUtils

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_form_personal.*

class FormPersonalFragment : Fragment() {
    private lateinit var listener: FormPersonalCallback
    private lateinit var urlIDCard: String
    private lateinit var urlSelfie: String
    private val cameraIDCard = 100
    private val cameraSelfie = 101
    companion object {
        fun newInstance(): FormPersonalFragment {
            val args: Bundle = Bundle()
            val fragment = FormPersonalFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener    = context as FormPersonalCallback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        urlIDCard = ""
        urlSelfie = ""
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_form_personal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(back_personal).subscribe{ listener.personalBack() }
        RxView.clicks(next_personal).subscribe {
            if (fullname.text.toString().isEmpty().or(phone_number.text.toString().isEmpty().or(urlIDCard.isEmpty().or(urlSelfie.isEmpty())))) {
                Toast.makeText(activity, "Please fill the field", Toast.LENGTH_LONG).show()
                return@subscribe
            }
            listener.personalNext(urlIDCard, urlSelfie)
        }
        lay_ktp.setOnClickListener {
            if (PermissionUtils.requestPermission(activity!!, 201, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                val intent = Intent(context, CameraBankActivity::class.java)
                startActivityForResult(intent, cameraIDCard)
            }
        }
        lay_swa.setOnClickListener {
            if (PermissionUtils.requestPermission(activity!!, 201, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                val intent = Intent(context, CameraSelfieActivity::class.java)
                startActivityForResult(intent, cameraSelfie)
            }
        }
    }

    interface FormPersonalCallback{
        fun personalBack()
        fun personalNext(urlIDCard: String, urlPhoto: String)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == cameraIDCard) {

        }
        when(requestCode) {
            cameraIDCard -> {
                if (resultCode == Activity.RESULT_OK) {
                    urlIDCard = data!!.getStringExtra("result")
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(context, "You not take picture", Toast.LENGTH_LONG).show()
                }
            }
            cameraSelfie -> {
                if (resultCode == Activity.RESULT_OK) {
                    urlSelfie = data!!.getStringExtra("result")
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(context, "You not take picture", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (PermissionUtils.permissionGranted(requestCode, 201, grantResults)) {
            val intent = Intent(context, CameraBankActivity::class.java)
            startActivityForResult(intent, cameraIDCard)
        }
    }
}

package id.privy.bankprivy.step.document


import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Base64
import android.util.Log
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout


import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import id.privy.bankprivy.R
import id.privy.bankprivy.utils.AspectRatioDevice

/**
 * A simple [Fragment] subclass.
 */
class ViewDocumentFragment : Fragment() {
    private var bitmapDocument: Bitmap? = null
    private var signaturebitmap: Bitmap? = null
    private var imgDoc: SubsamplingScaleImageView? = null
    private var typepotrait: String? = null
    private var signatureBase64: String? = null
    private var sizexcanvas: Int = 0
    private var sizeycanvas: Int = 0
    private var header: Int = 0
    private var idimage = 0
    private var sizePdfx: Float = 0.toFloat()
    private var sizePdfy: Float = 0.toFloat()
    private var sizefinalx: Float = 0.toFloat()
    private var sizefinaly: Float = 0.toFloat()
    private var rasiox: Float = 0.toFloat()
    private var rasioy: Float = 0.toFloat()

    private lateinit var itemDocument: RelativeLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bitmapDocument = arguments!!.getParcelable("bitmapDocument")
        sizePdfx = arguments!!.getFloat("pdfx")
        sizePdfy = arguments!!.getFloat("pdfy")
        if (sizePdfx < sizePdfy) {
            typepotrait = "p"
        } else {
            typepotrait = "l"
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.item_view_document, container, false)
        itemDocument = view.findViewById(R.id.layCanvas)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (typepotrait == "p") {
            imgDoc = SubsamplingScaleImageView(activity!!)
            imgDoc!!.layoutParams = AspectRatioDevice(activity).aspectRationPotrait
            imgDoc!!.setImage(ImageSource.bitmap(bitmapDocument!!))
            itemDocument.addView(imgDoc)

        } else {
            imgDoc = SubsamplingScaleImageView(activity!!)
            imgDoc!!.layoutParams = AspectRatioDevice(activity).aspectRationLanscape
            imgDoc!!.setImage(ImageSource.bitmap(bitmapDocument!!))
            itemDocument.addView(imgDoc)

        }
    }

    fun addSignatureCanvas() {
        if (idimage == 0) {
            imgDoc!!.resetScaleAndCenter()
            idimage = 1
            getRatio()
        }
    }

    private fun getRatio() {
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        sizexcanvas = size.x - itemDocument.width
        sizeycanvas = size.y - itemDocument.height

        header = 0
        signaturebitmap = base64ToBitmap(signatureBase64)

        rasiox = itemDocument.width.toFloat() / sizePdfx
        rasioy = itemDocument.height.toFloat() / sizePdfy
    }


    private fun base64ToBitmap(b64: String?): Bitmap? {
        try {
            val imageAsBytes = Base64.decode(b64!!.toByteArray(), Base64.DEFAULT)
            val bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.size)

            if (bitmap != null) {
                if (typepotrait == "p") {
                    //potrait
                    //Log.e("data","p");
                    val signx = (itemDocument.width.toFloat() * 0.25).toFloat()
                    val signy = (itemDocument.height.toFloat() * 0.10).toFloat()
                    return Bitmap.createScaledBitmap(bitmap, Integer.valueOf(signx.toInt())!!, Integer.valueOf(signy.toInt())!!, false)
                } else {
                    //lanscape
                    // Log.e("data","l");
                    val signx = (itemDocument.width.toFloat() * 0.18).toFloat()
                    val signy = (itemDocument.height.toFloat() * 0.12).toFloat()
                    return Bitmap.createScaledBitmap(bitmap, Integer.valueOf(signx.toInt())!!, Integer.valueOf(signy.toInt())!!, false)
                }
            }
            return bitmap
        } catch (e: Exception) {
            return null
        }

    }


    companion object {

        fun newInstance(bitmap: Bitmap, sizePdfx: Float, sizePdfy: Float): ViewDocumentFragment {
            val args = Bundle()
            args.putParcelable("bitmapDocument", bitmap)
            args.putFloat("pdfx", sizePdfx)
            args.putFloat("pdfy", sizePdfy)
            val fragment = ViewDocumentFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

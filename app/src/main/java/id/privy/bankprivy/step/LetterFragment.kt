package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.necistudio.vigerpdf.VigerPDF
import com.necistudio.vigerpdf.item.Data
import com.necistudio.vigerpdf.manage.OnResultListener
import id.privy.bankprivy.step.document.ViewDocumentFragment

import id.privy.bankprivy.R
import id.privy.bankprivy.utils.ViewPagerDocumentAdapter
import kotlinx.android.synthetic.main.fragment_letter.*
import java.util.ArrayList

class LetterFragment : Fragment() {
    private lateinit var listener: LetterCallback
    private var viewPagerAdapter: ViewPagerDocumentAdapter? = null
    private lateinit var itemDocument: MutableList<Fragment>
    private lateinit var vigerPDF: VigerPDF

    companion object {
        fun newInstance(): LetterFragment {
            val args: Bundle = Bundle()
            val fragment = LetterFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as LetterCallback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vigerPDF = VigerPDF()
        vigerPDF.clearCache()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_letter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(letter_next).subscribe { listener.signLetter() }

        getDocument("http://www.pdf995.com/samples/pdf.pdf")
    }

    interface LetterCallback {
        fun signLetter()
    }

    override fun onResume() {
        super.onResume()
        vigerPDF.onCancle()
        getDocument("http://www.pdf995.com/samples/pdf.pdf")
    }

    fun getDocument(endpoint: String) {
        itemDocument = ArrayList()
        viewPagerAdapter = ViewPagerDocumentAdapter(fragmentManager, itemDocument)
        viewPagerDocument.adapter = viewPagerAdapter
        vigerPDF.vigerFromNetwork(endpoint, object : OnResultListener {
            override fun resultData(data: Data) {
                itemDocument.add(ViewDocumentFragment.newInstance(data.bitmap, java.lang.Float.valueOf(data.width)!!, java.lang.Float.valueOf(data.height)!!))
                viewPagerAdapter!!.notifyDataSetChanged()
            }

            override fun progressData(progress: Int) {
                Log.e("data", "" + progress)
            }

            override fun failed(t: Throwable) {
                Log.e("data", "" + t.message)
            }

            override fun finish(datai: Data) {
            }
        })

    }
}

package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_welcome.*

class WelcomeFragment : Fragment() {
    lateinit var listener: WelcomeCallback
    companion object {
        fun newInstance(): WelcomeFragment {
            val args: Bundle = Bundle()
            val fragment = WelcomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(sign_up).subscribe { listener.signUp() }
        RxView.clicks(sign_in).subscribe { listener.signIn() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener    = context as WelcomeCallback
    }

    interface WelcomeCallback{
        fun signIn()
        fun signUp()
    }
}

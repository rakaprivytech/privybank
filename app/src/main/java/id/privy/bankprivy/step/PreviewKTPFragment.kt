package id.privy.bankprivy.step


import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_preview_kt.*

class PreviewKTPFragment : Fragment() {
    private lateinit var listener: PreviewKTPCallback
    companion object {
        fun newInstance(urlIDCard: String, urlPhoto: String): PreviewKTPFragment {
            val args: Bundle = Bundle()
            args.putString("urlCard", urlIDCard)
            args.putString("urlPhoto", urlPhoto)
            val fragment = PreviewKTPFragment()
            fragment.arguments = args
            return fragment
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_preview_kt, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener    = context as PreviewKTPCallback
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(back_viewktp).subscribe { listener.backKtp() }
        RxView.clicks(next_viewktp).subscribe { listener.nextKtp() }
        if (arguments!!.getString("urlCard").isNotEmpty()) {
            view_ktp.setImageURI(Uri.parse(arguments!!.getString("urlCard")))
        }
    }

    interface PreviewKTPCallback{
        fun backKtp()
        fun nextKtp()
    }
}

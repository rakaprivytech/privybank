package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_job.*

class JobFragment : Fragment() {
    private lateinit var listener: JobCallback
    companion object {
        fun newInstance(): JobFragment {
            val args: Bundle = Bundle()
            val fragment = JobFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as JobCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_job, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(next_job).subscribe { listener.nextJob() }
        RxView.clicks(back_job).subscribe { listener.backJob() }
    }
    interface JobCallback{
        fun backJob()
        fun nextJob()
    }
}

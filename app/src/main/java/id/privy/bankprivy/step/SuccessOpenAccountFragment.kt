package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
class SuccessOpenAccountFragment : Fragment() {
    private lateinit var listener: SuccessOpenCallback
    companion object {
        fun newInstance(): SuccessOpenAccountFragment {
            val args: Bundle = Bundle()
            val fragment = SuccessOpenAccountFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as SuccessOpenCallback
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_success_open_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    interface SuccessOpenCallback{
        fun successCreated()
    }
}

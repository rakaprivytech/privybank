package id.privy.bankprivy.step


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView

import id.privy.bankprivy.R
import kotlinx.android.synthetic.main.fragment_verified.*

class VerifiedFragment : Fragment() {
    lateinit var listener: VerifiedCallback
    companion object {
        fun newInstance(): VerifiedFragment {
            val args: Bundle = Bundle()
            val fragment = VerifiedFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as VerifiedCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_verified, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxView.clicks(next_verified).subscribe { listener.nextVerified() }
        RxView.clicks(back_verified).subscribe { listener.backVerified() }
    }

    interface VerifiedCallback{
        fun backVerified()
        fun nextVerified()
    }
}
